﻿using System;

namespace Domain.Models
{
    /// <summary>
    /// Model for the stock item class
    /// </summary>
    public class Stock
    {
        public string Symbol { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan Time { get; set; }
        public float Open { get; set; }
        public float High { get; set; }
        public float Low { get; set; }
        public float Close { get; set; }
        public float Volume { get; set; }
    }
}
