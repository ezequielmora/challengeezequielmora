﻿namespace Domain.Models
{
    /// <summary>
    /// Enum for the different states of the commands
    /// </summary>
    public enum IsSuccessful
    {
        Fail,
        Command,
        Help
    }

    /// <summary>
    /// Model with the properties of possible API responses
    /// </summary>
    public class ApiResponse
    {
        public IsSuccessful IsSuccessful;
        public bool Detected { get; set; }
        public string ErrorMessage { get; set; }
        public string Symbol { get; set; }
        public float ClosePrice { get; set; }
    }
}
