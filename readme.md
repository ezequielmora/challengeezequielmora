# **This is the Jobsity Challenge**  
Open it with Visual Studio 2019  
It uses SignalR for message broker  
It uses Test with NUnit for validate communication with chatbot - Live Testing must be enabled  
It contains a custom bot made in net core for request using the command **/stock=aapl.us** also added the command **/help**   
For Run you must Run the Chat project and the ChatBot project at the same time or use multiple Visual Studio Instances      
For Run you must type the following command in the package manager console: update-database  
**NOTE: The project require the NET CORE 2.2 RUNTIME INSTALLED TO WORK PROPERLY**
