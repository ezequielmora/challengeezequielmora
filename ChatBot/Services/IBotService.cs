﻿using Domain.Models;

namespace ChatBot.Services
{
    /// <summary>
    /// Interface for the bot service
    /// </summary>
    public interface IBotService
    {
        Stock GetStock(string inputCode);
    }
}
