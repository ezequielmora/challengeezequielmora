﻿using System;
using ChatBot.Services;
using Microsoft.AspNetCore.Mvc;
using Domain.Models;

namespace ChatBot.Controllers
{
    /// <summary>
    /// ChatBot controller for the main Bot API
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ChatBotController : ControllerBase
    {
        private IBotService retrieveService;
        /// <summary>
        /// Constructor that will be called 
        /// </summary>
        /// <param name="serviceInstace"></param>
        public ChatBotController(IBotService serviceInstace)
        {
            retrieveService = serviceInstace;
        }
        /// <summary>
        /// Main Get request to get the Stock Value
        /// </summary>
        /// <param name="inputCode"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStock")]
        public ActionResult<Stock> GetStock(string inputCode)
        {
            try
            {
                var response = retrieveService.GetStock(inputCode);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}