﻿using Domain.Models;

namespace ChatJobsityTask.Services
{
    /// <summary>
    /// Interface for the bot connection service
    /// </summary>
    public interface IBotConnectionService
    {
        ApiResponse BotDetection(string message);
    }
}
