﻿using Domain.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;

namespace ChatJobsityTask.Services
{
    public class BotConnectionService : IBotConnectionService
    {
        private HttpClient client { get; set; }

        /// <summary>
        /// Constructor that will be called
        /// </summary>
        /// <param name="_client"></param>
        public BotConnectionService(HttpClient _client)
        {
            client = _client;
        }

        /// <summary>
        /// Main method that detect the input commands into the chat box and return different responses depending the message value
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public ApiResponse BotDetection(string message)
        {
            try
            {
                if (message.ToLower().Contains("/stock="))
                {
                    string code = message.Replace("/stock=", "");
                    using (HttpResponseMessage response = client.GetAsync($"http://localhost:65445/api/ChatBot/GetStock?inputCode={code}").Result)
                    using (HttpContent content = response.Content)
                    {
                        string serviceResponse = content.ReadAsStringAsync().Result;
                        if (response.StatusCode != System.Net.HttpStatusCode.OK)
                        {
                            return new ApiResponse { Detected = true, IsSuccessful = IsSuccessful.Fail, ErrorMessage = response.StatusCode.ToString() };
                        }

                        var stock = JsonConvert.DeserializeObject<Stock>(serviceResponse);
                        return new ApiResponse { Detected = true, IsSuccessful = IsSuccessful.Command, Symbol = stock.Symbol, ClosePrice = stock.Close };
                    }
                }
                if (message.ToLower().Contains("/help"))
                {
                    return new ApiResponse { Detected = true, IsSuccessful = IsSuccessful.Help};
                }

                return new ApiResponse { Detected = false };
            }
            catch (Exception ex)
            {
                return new ApiResponse { Detected = true, IsSuccessful = IsSuccessful.Fail, ErrorMessage = ex.Message };
            }
        }
    }
}
