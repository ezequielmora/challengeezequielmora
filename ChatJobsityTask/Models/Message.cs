﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ChatJobsityTask.Models
{
    /// <summary>
    /// Different properties of the model for the Message Item
    /// </summary>
    public class Message
    {
        public int Id { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Text { get; set; }

        public DateTime When { get; set; }

        public string UserID { get; set; }

        public virtual AppUser Sender { get; set; }

        public Message()
        {
            When = DateTime.Now;
        }
    }
}
