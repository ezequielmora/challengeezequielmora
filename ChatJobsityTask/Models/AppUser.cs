﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace ChatJobsityTask.Models
{
    /// <summary>
    /// Class that handle the user inherit from IdentityUser plugin
    /// </summary>
    public class AppUser : IdentityUser
    {
        public AppUser()
        {
            Messages = new HashSet<Message>();
        }

        public virtual ICollection<Message> Messages { get; set; }
    }
}
