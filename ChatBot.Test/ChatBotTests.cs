using NUnit.Framework;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ChatBot.Test
{
    public class Tests
    {
        /// <summary>
        /// Test method to test if the connection with the bot success
        /// </summary>
        /// <returns>Ok</returns>
        [Test]
        public async Task GetValue_Ok()
        {
            var uri = new Uri("http://localhost:65445/api/ChatBot/GetStock?inputCode=aapl.us");
            var client = new HttpClient { MaxResponseContentBufferSize = 256000 };

            HttpResponseMessage response = await client.GetAsync(uri);
            var sucess = response.IsSuccessStatusCode;
            Assert.True(sucess);
        }

        /// <summary>
        /// Test method to test if the connection fails
        /// </summary>
        /// <returns>Not found</returns>
        [Test]
        public async Task GetValue_Error()
        {
            var uri = new Uri("http://localhost:65445/api/ChatBot/GetStock?inputCode=");
            var client = new HttpClient { MaxResponseContentBufferSize = 256000 };

            HttpResponseMessage response = await client.GetAsync(uri);
            var sucess = response.IsSuccessStatusCode;
            Assert.False(sucess);
        }
    }
}